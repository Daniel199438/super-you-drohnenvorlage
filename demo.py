from template.drone import DroneController
import time


def square_flight(drone: DroneController, travel_distance_cm: int, speed: int):
    # Fly in a square pattern
    for i in range(4):
        print(f"Side {i + 1} of the square")

        # Fly forward
        drone.go_xyz_speed(travel_distance_cm, 0, 0, speed)
        time.sleep(0.5)

        # Rotate 90 degrees clockwise
        drone.rotate_clockwise(90)
        time.sleep(0.5)


def spiral_flight(drone: DroneController, travel_distance_cm: int, speed: int):
    # Fly in a spiral pattern
    for i in range(1, 11):
        print(f"Spirale {i}")

        # Fly upwards
        drone.go_xyz_speed(0, 0, i * travel_distance_cm, speed)
        time.sleep(0.5)

        # Fly to the right
        drone.go_xyz_speed(0, i * travel_distance_cm, 0, speed)
        time.sleep(0.5)

        # Fly downwards
        drone.go_xyz_speed(0, 0, -i * travel_distance_cm, speed)
        time.sleep(0.5)

        # Fly to the left
        drone.go_xyz_speed(0, -i * travel_distance_cm, 0, speed)
        time.sleep(0.5)