from datetime import datetime

import cv2
from ultralytics import YOLO


# 1. pip install ultralytics

def generate_unique_filename(preposition='output_video'):
    now = datetime.now()
    timestamp = datetime.timestamp(now)
    milliseconds = int(timestamp * 1000)
    date_string = now.strftime("%Y-%m-%d_%H-%M-%S")
    unique_filename = f"{preposition}_{date_string}_{milliseconds}.mp4"
    return unique_filename


cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)

# model = YOLO('model/yolov8s-seg.pt', task='segment')
model = YOLO('model/yolov8s.pt', task='detect')

now = datetime.now()
date_string = now.strftime("%Y-%m-%d_%H-%M-%S")

writer = cv2.VideoWriter(f'recordings/test_{date_string}.mp4',
                         cv2.VideoWriter_fourcc(*'mp4v'),
                         15, (640, 480))

writer_ai = cv2.VideoWriter(f'recordings/test_ai_{date_string}.mp4',
                            cv2.VideoWriter_fourcc(*'mp4v'),
                            15, (640, 480))

screenshot_counter = 0

while True:
    ret, img = cap.read()

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    writer.write(img)

    results = model(img, conf=0.5)

    annotated_frame = results[0].plot()

    cv2.imshow('Webcam', annotated_frame)

    writer_ai.write(annotated_frame)

    key = cv2.waitKey(1)

    if key == ord('q'):
        break
    elif key == ord(' '):  # Leertaste wurde gedrückt
        cv2.imwrite(f'screenshot_{screenshot_counter}.png', annotated_frame)
        screenshot_counter += 1
cap.release()
writer.release()
writer_ai.release()
cv2.destroyAllWindows()
