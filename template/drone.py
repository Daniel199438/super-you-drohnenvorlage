# project.py
import threading
import time
from datetime import datetime

import cv2
import keyboard
from djitellopy import Tello
from ultralytics import YOLO


class DroneController:
    def __init__(self):
        self.model = YOLO('model/yolov8s.pt', task='detect')
        self.auto_record = False
        self.current_ai_results = None
        self.tello = Tello()
        self.tello.connect()
        self.tello.streamon()
        self.listen_for_emergency_stop()
        self.start_video_stream_thread()

    def emergency_stop(self, e):
        print("Notaus!")
        self.tello.emergency()

    def start_video_stream(self):
        cv2.namedWindow("Tello Video Stream", cv2.WINDOW_NORMAL)

        # Startzeit festlegen
        start_time = time.time()
        if self.auto_record:
            fourcc = cv2.VideoWriter_fourcc(*'mp4v')
            now = datetime.now()
            date_string = now.strftime("%Y-%m-%d_%H-%M-%S")
            writer = cv2.VideoWriter(f"recordings/flight_{date_string}", fourcc, 20.0, (640, 480))
            writer_ai = cv2.VideoWriter(f"recordings/flight_ai_{date_string}", fourcc, 20.0, (640, 480))

        while True:
            frame_read = self.tello.get_frame_read()

            if frame_read.stopped:
                break

            frame = frame_read.frame

            if frame is not None:
                # Flugzeit berechnen
                flight_time = time.time() - start_time

                # Batterieladestand abrufen
                battery_level = self.get_battery()

                # Geschwindigkeit und Flughöhe setzen (diese Werte sollten aus Ihren Befehlen abgerufen werden)
                speed = f"x: {self.tello.get_speed_x()} m/s\n" \
                        f"y: {self.tello.get_speed_y()} m/s\n" \
                        f"z: {self.tello.get_speed_z()} m/s\n"

                altitude = self.tello.get_height()

                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                if self.auto_record:
                    writer.write(frame)

                self.current_ai_results = self.model(frame, conf=0.5)

                # Visualize the results on the frame
                annotated_frame = self.current_ai_results[0].plot()

                # Convert the annotated frame colors from BGR to RGB
                annotated_frame = cv2.cvtColor(annotated_frame, cv2.COLOR_BGR2RGB)

                if self.auto_record:
                    writer_ai.write(annotated_frame)

                # Informationen zum Frame hinzufügen
                cv2.putText(annotated_frame, f"Geschwindigkeit: {speed}", (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 255, 0), 2)
                cv2.putText(annotated_frame, f"Flughöhe: {altitude}", (10, 90), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 255, 0),
                            2)
                cv2.putText(annotated_frame, f"Batterie: {battery_level}%", (10, 150), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 255, 0), 2)
                cv2.putText(annotated_frame, f"Flugzeit: {flight_time:.2f}s", (10, 210), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (0, 255, 0), 2)

                cv2.imshow("Tello Video Stream", annotated_frame)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

        if self.auto_record:
            writer.release()
            writer_ai.release()
        cv2.destroyAllWindows()

    def start_video_stream_thread(self):
        video_thread = threading.Thread(target=self.start_video_stream)
        video_thread.start()

    def listen_for_emergency_stop(self):
        keyboard.on_press_key("space", self.emergency_stop)

    def takeoff(self):
        self.tello.takeoff()

    def land(self):
        self.tello.land()

    def move_left(self, distance):
        self.tello.move_left(distance)

    def move_right(self, distance):
        self.tello.move_right(distance)

    def move_up(self, distance):
        self.tello.move_up(distance)

    def move_down(self, distance):
        self.tello.move_down(distance)

    def move_forward(self, distance):
        self.tello.move_forward(distance)

    def move_back(self, distance):
        self.tello.move_back(distance)

    def flip_forward(self):
        self.tello.flip_forward()

    def flip_back(self):
        self.tello.flip_back()

    def flip_left(self):
        self.tello.flip_left()

    def flip_right(self):
        self.tello.flip_right()

    def set_speed(self, speed):
        self.tello.set_speed(speed)

    def rotate_clockwise(self, angle):
        self.tello.rotate_clockwise(angle)

    def rotate_counter_clockwise(self, angle):
        self.tello.rotate_counter_clockwise(angle)

    def get_battery(self):
        return self.tello.get_battery()

    def go_xyz_speed(self, x, y, z, speed):
        self.tello.go_xyz_speed(x, y, z, speed)

    def get_height(self):
        return self.tello.get_height()

    def is_person_in_video(self):
        if self.current_ai_results is not None:
            for result in self.current_ai_results:
                for box in result.boxes:
                    class_id = int(box.data[0][-1])
                    if self.model.names[class_id] == "person":
                        return True
        return False

    def is_object_in_video(self, object_name: str):
        if self.current_ai_results is not None:
            for result in self.current_ai_results:
                for box in result.boxes:
                    class_id = int(box.data[0][-1])
                    if self.model.names[class_id] == object_name:
                        return True
        return False


    def get_objects_from_video(self):
        if self.current_ai_results is not None:
            objects = []
            for result in self.current_ai_results:
                for box in result.boxes:
                    class_id = int(box.data[0][-1])
                    objects.append(self.model.names[class_id])
            return objects
        return []
